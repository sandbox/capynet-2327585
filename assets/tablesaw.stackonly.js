;
(function ($) {
  Drupal.behaviors.responsiveTableStack = {
    attach: function (context, settings) {
      ////////////////////////////////////

      /**
       *
       * @param element
       * @constructor
       */
      var Stack = function (element) {

        this.cssClasses = {
          stackTable: 'tablesaw-stack',
          cellLabels: 'tablesaw-cell-label',
          cellContentLabels: 'tablesaw-cell-content'
        };

        this.excludedTRs = '.module';

        this.$table = $(element);

        this.$table.addClass(this.cssClasses.stackTable);

        this.allHeaders = this.$table.find("th");
        this.$table.data({
          obj: 'tablesaw-stack'
        }, this);
      };

      Stack.prototype = {

        /**
         *
         * @returns {Number}
         * @private
         */
        _initCells: function () {
          var colsQty;
          var trs = this.$table.find("thead tr");
          var self = this;

          $(trs).each(function () {
            var colsCnt = 0;

            $(this).children().each(function () {
              var th = this;
              var hasColSpan = parseInt(this.getAttribute("colspan"), 10);
              var sel = ":nth-child(" + ( colsCnt + 1 ) + ")";

              colsQty = colsCnt + 1;

              // Si tiene un colspan lo preprocesamos.
              if (hasColSpan) {
                for (var k = 0; k < hasColSpan - 1; k++) {
                  colsCnt++;
                  sel += ", :nth-child(" + ( colsCnt + 1 ) + ")";
                }
              }

              // Store "cells" data on header as a reference to all cells in the same column as this TH
              th.cells = self.$table.find("tr").not($(trs).eq(0)).not(this).children(sel);
              colsCnt++;
            });
          });

          return colsQty;
        },

        /**
         *
         */
        init: function () {
          var self = this;
          this._initCells();

          // get headers in reverse order so that top-level headers are appended last
          var reverseHeaders = $(this.allHeaders);

          // create the hide/show toggles
          reverseHeaders.each(function () {
            var $cells = $(this.cells);
            var text = $(this).text();

            if (text !== "") {
              // @todo tal vez podria usar un Drupal.theme()
              $cells.not(self.excludedTRs)
                .wrapInner("<span class='" + self.cssClasses.cellContentLabels + "'></span>")
                .prepend("<b class='" + self.cssClasses.cellLabels + "'>" + text + "</b>");
            }
          });
        }


      };

      // Inicializamos todo.
      var $tables = $('table');
      $tables.once('table-stack', function () {
        new Stack(this).init();
      });


      ////////////////////////////////////
    }
  };

})(jQuery);

